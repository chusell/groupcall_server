package org.kurento.tutorial.groupcall;

import java.util.ArrayList;
import java.util.List;

public class RoomPort {
  private final ArrayList<Boolean> ports = new ArrayList();

  private final int startPort;
  private final int endPort;
  private final int size;

  public RoomPort(int start, int end) {
    this.startPort = start;
    this.endPort = end;
    this.size = (end - start) + 1;

    for(int i=0; i<this.size; i++) {
      ports.add( false ); // not used
    }
  }

  synchronized public int getPort() {
    int port = -1;

    for(int offset=0; offset<size; offset++) {
      if (ports.get(offset) == false) {
        port = (this.startPort + offset);
        ports.set(offset, true); // set used
        break;
      }
    }

    return port;
  }

  synchronized public void removePort(int port) {
    int offset = port - this.startPort;
    ports.set(offset, false);
  }

  synchronized public int startPort() {
    return this.startPort;
  }

  synchronized public int endPort() {
    return this.endPort;
  }

  synchronized public int size() {
    return this.size;
  }
}
