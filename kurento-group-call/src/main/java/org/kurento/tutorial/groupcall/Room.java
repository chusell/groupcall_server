/*
 * (C) Copyright 2014 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.kurento.tutorial.groupcall;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Date;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * @author Ivan Gracia (izanmail@gmail.com)
 * @since 4.3.1
 */
public class Room implements Closeable {
  private final Logger log = LoggerFactory.getLogger(Room.class);

  private final ConcurrentMap<String, UserSession> participants = new ConcurrentHashMap<>();
  private final String name;

  private final Object lock = new Object();
  private final CopyOnWriteArrayList<UserSession> speakingUsers = new CopyOnWriteArrayList();
  private final CopyOnWriteArrayList<UserSession> candidateSpeakingUsers = new CopyOnWriteArrayList();
  private static final int MAX_COUNT_SPEAKING_USER = 1000; // do not use candidate array

  private RoomPort roomPort;
  private final ConcurrentMap<String, Integer> ports = new ConcurrentHashMap<>();

  private final Timer jobScheduler = new Timer();

  public String getName() {
    return name;
  }

  public Room(String roomName, RoomPort roomPort) {
    this.name = roomName;
    this.roomPort = roomPort;
    log.info("ROOM {} has been created", roomName);

    startSpeechUserTimer(true);
  }

  @PreDestroy
  private void shutdown() {
    this.close();
  }

  public UserSession join(String userName, WebSocketSession session) throws IOException {
    log.info("USER [{}]: Join room {}", userName, this.name);
    final int port = createPort(userName);

    final UserSession participant = new UserSession(userName, this.name, session, port);
    joinRoom(participant);
    participants.put(participant.getName(), participant);
    sendParticipantNames(participant);
    return participant;
  }

  public void leave(UserSession user) throws IOException {
    log.info("USER [{}]: Leaving room {}", user.getName(), this.name);
    this.removeParticipant(user.getName());
    this.removePort(user.getName());
    user.close();
  }

  public void broadcastMessage(UserSession user, String message) throws IOException {
    log.info("ROOM {}: USER [{}] Broadcast Message", this.name, user.getName());
    final JsonObject broadcastMessage = new JsonObject();
    broadcastMessage.addProperty("id", "broadcastMessage");
    broadcastMessage.addProperty("name", user.getName());
    broadcastMessage.addProperty("message", message);
    for (final UserSession participant : this.getParticipants()) {
      if (!participant.equals(user)) {
        participant.sendMessage(broadcastMessage);
      }
    }
  }

  public void broadcastVideoShare(UserSession user, String videoId) throws IOException {
    log.info("ROOM {}: USER [{}]: Broadcast Video Share", this.name, user.getName());
    final JsonObject broadcastVideoShare = new JsonObject();
    broadcastVideoShare.addProperty("id", "broadcastVideoShare");
    broadcastVideoShare.addProperty("name", user.getName());
    broadcastVideoShare.addProperty("video_id", videoId);
    for (final UserSession participant : this.getParticipants()) {
      if (!participant.equals(user)) {
        participant.sendMessage(broadcastVideoShare);
      }
    }
  }

  public void broadcastEmoticon(UserSession user, String name, int emoticonId) throws IOException {
    log.info("ROOM {}: USER [{}]: Broadcast Emoticon", this.name, user.getName());
    final JsonObject broadcastEmoticon = new JsonObject();
    broadcastEmoticon.addProperty("id", "broadcastEmoticon");
    broadcastEmoticon.addProperty("name", name);
    broadcastEmoticon.addProperty("emoticon_id", emoticonId);
    for (final UserSession participant : this.getParticipants()) {
      if (!participant.equals(user)) {
        participant.sendMessage(broadcastEmoticon);
      }
    }
  }

  public void onRequestSpeakingUser(UserSession user) throws IOException {
    synchronized (lock) {
      if (speakingUsers.size() < MAX_COUNT_SPEAKING_USER) {
        log.debug("ROOM {}: USER [{}] speech", name, user.getName());
        speakingUsers.add(user);
      } else {
        candidateSpeakingUsers.add(user);
      }
    }
  }

  public void onStopSpeakingUser(UserSession user) throws IOException {
    synchronized (lock) {
      boolean findSpeakingUser = false;
      for(UserSession su : speakingUsers) {
        if (user == su) {
          findSpeakingUser = true;
        }
      }

      // SpeakingUsers 에 있는지 확인 -> 있으면 삭제
      log.debug("ROOM {}: USER [{}] speech deactive", name, user.getName());
      if (findSpeakingUser) {
        speakingUsers.remove(user);

        // 첫번째 후보유저를 SpeakingUsers 로 옮긴다
        if (candidateSpeakingUsers.size() > 0) {
          final UserSession firstUser = candidateSpeakingUsers.remove(0);

          speakingUsers.add(firstUser);
        }
      } else {
        // SpeakingUsers 에는 존재하지 않음 -> 후보군에 있는지 확인해서 삭제
        boolean findCandidateSpeakingUser = false;
        for(UserSession cu : candidateSpeakingUsers) {
          if (user == cu) {
            findCandidateSpeakingUser = true;
          }
        }

        if (findCandidateSpeakingUser) {
          candidateSpeakingUsers.remove(user);
        }
      }
    }
  }

  public RoomPort getRoomPort() {
    return this.roomPort;
  }

  //
  // Privates
  //
  private int createPort(String userName) {
    int port = this.roomPort.getPort();
    ports.put(userName, port);

    if (port == -1) {
      log.error("ROOM {}: room has not available port of participant [{}]", this.name, userName);
    }
    return port;
  }

  private void removePort(String userName) {
    Integer port = this.ports.get(userName);
    if (port != null) {
      this.roomPort.removePort(port);
    }
    ports.remove(userName);
  }

  private Collection<String> joinRoom(UserSession newParticipant) throws IOException {
    final JsonObject newParticipantMsg = new JsonObject();
    newParticipantMsg.addProperty("id", "newParticipantArrived");
    newParticipantMsg.addProperty("name", newParticipant.getName());

    final List<String> participantsList = new ArrayList<>(participants.values().size());
    log.debug("ROOM {}: notifying other participants of new participant [{}]", name, newParticipant.getName());

    for (final UserSession participant : participants.values()) {
      try {
        participant.sendMessage(newParticipantMsg);
      } catch (final IOException e) {
        log.debug("ROOM {}: participant [{}] could not be notified", name, participant.getName(), e);
      }
      participantsList.add(participant.getName());
    }

    return participantsList;
  }

  private void removeParticipant(String name) throws IOException {
    final UserSession us = this.getParticipant(name);
    if (us == null) {
      log.debug("ROOM {}: {} is not existing", this.name, name);
      return;
    }

    participants.remove(name);

    log.debug("ROOM {}: notifying all users that {} is leaving the room", this.name, name);

    final List<String> unnotifiedParticipants = new ArrayList<>();
    final JsonObject participantLeftJson = new JsonObject();
    participantLeftJson.addProperty("id", "participantLeft");
    participantLeftJson.addProperty("name", name);
    for (final UserSession participant : participants.values()) {
      try {
        participant.cancelVideoFrom(name);
        participant.sendMessage(participantLeftJson);
      } catch (final IOException e) {
        unnotifiedParticipants.add(participant.getName());
      }
    }

    if (!unnotifiedParticipants.isEmpty()) {
      log.debug("ROOM {}: The users {} could not be notified that {} left the room", this.name,
          unnotifiedParticipants, name);
    }

  }

  private void sendParticipantNames(UserSession user) throws IOException {
    final JsonArray participantsArray = new JsonArray();
    for (final UserSession participant : this.getParticipants()) {
      if (!participant.equals(user)) {
        final JsonElement participantName = new JsonPrimitive(participant.getName());
        participantsArray.add(participantName);
      }
    }

    final JsonObject existingParticipantsMsg = new JsonObject();
    existingParticipantsMsg.addProperty("id", "existingParticipants");
    existingParticipantsMsg.add("data", participantsArray);
    log.debug("PARTICIPANT {}: sending a list of {} participants", user.getName(), participantsArray.size());
    user.sendMessage(existingParticipantsMsg);
  }

  public void sendSpeechUsers(ArrayList<String> list) throws IOException {
    final JsonArray userList = new JsonArray();
    for (String userName : list) {
      final JsonElement jName = new JsonPrimitive(userName);
      userList.add(jName);
    }
    
    final JsonObject msg = new JsonObject();
    msg.addProperty("id", "speechUserList");
    msg.add("list", userList);

    for (final UserSession participant : this.getParticipants()) {
      participant.sendMessage(msg);
    }
  }

  //
  // API
  //
  public Collection<UserSession> getParticipants() {
    return participants.values();
  }

  public UserSession getParticipant(String name) {
    return participants.get(name);
  }

  @Override
  public void close() {
    for (final UserSession user : participants.values()) {
      try {
        user.close();
      } catch (IOException e) {
        log.debug("ROOM {}: Could not invoke close on participant [{}]", this.name, user.getName(), e);
      }
    }

    for (int port : ports.values()) {
      roomPort.removePort(port);
    }

    participants.clear();
    ports.clear();
    startSpeechUserTimer(false);

    log.debug("Room {} closed", this.name);
  }

  private void startSpeechUserTimer(boolean start) {
    if (start) {
      final ScheduledJob job = new ScheduledJob();
      jobScheduler.scheduleAtFixedRate(job, 2000, 2000);
    } else {
      jobScheduler.cancel();
    }
  }

  class ScheduledJob extends TimerTask {
    public void run() {
      final ArrayList<String> list = new ArrayList();
      synchronized (lock) {
        for(UserSession su : Room.this.speakingUsers) {
          list.add(su.getName());
        }
      }
      log.info("ROOM {}: Speech Users {}", Room.this.name, list);

      try {
        Room.this.sendSpeechUsers(list);
      } catch (IOException e) {
        log.error("ROOM {}: send ERROR {}", Room.this.name, e);
      }
    }
  }
}
