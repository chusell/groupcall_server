/*
 * (C) Copyright 2014 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.kurento.tutorial.groupcall;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 *
 * @author Ivan Gracia (izanmail@gmail.com)
 * @since 4.3.1
 */
public class UserSession implements Closeable {

  private static final Logger log = LoggerFactory.getLogger(UserSession.class);

  private final String name;
  private final WebSocketSession session;

  private int port;
  private final String roomName;
  private final ArrayList<String> ssrcs;

  private static final String cname = "cname:PPzLtXwp4Ltu2/hc";
  private static final String audiomsid = "msid:ARDAMS ARDAMSa0";
  private static final String videomsid = "msid:ARDAMS ARDAMSv0";

  private String cryptoLine;

  public UserSession(final String name, String roomName, final WebSocketSession session, int port) {
    this.name = name;
    this.session = session;
    this.roomName = roomName;
    this.port = port;

    // creation ssrc
    this.ssrcs = new ArrayList<String>();

    int ssrc1 = ThreadLocalRandom.current().nextInt(1000000000, Integer.MAX_VALUE);
    int ssrc2 = ThreadLocalRandom.current().nextInt(1000000000, Integer.MAX_VALUE);
    int ssrc3 = ThreadLocalRandom.current().nextInt(1000000000, Integer.MAX_VALUE);

    ssrcs.add( String.valueOf(ssrc1) );
    ssrcs.add( String.valueOf(ssrc2) );
    ssrcs.add( String.valueOf(ssrc3) );

    log.info("USER [{}]: port [{}], ssrc [{}, {}, {}]", name, port, ssrcs.get(0), ssrcs.get(1), ssrcs.get(2));
  }

  public String getName() {
    return name;
  }

  public ArrayList<String> getSsrcs() {
    return ssrcs;
  }

  public String getCname() {
    return cname;
  }

  public String getAudiomsid() {
    return audiomsid;
  }

  public String getVideomsid() {
    return videomsid;
  }

  public WebSocketSession getSession() {
    return session;
  }

  /**
   * The room to which the user is currently attending.
   *
   * @return The room
   */
  public String getRoomName() {
    return this.roomName;
  }

  public void receiveVideoFrom(UserSession sender, String sdpOffer) throws IOException {
    // log.info("USER [{}]: connecting with [{}] in room {}", this.name, sender.getName(), this.roomName);
    log.trace("USER [{}]: SdpOffer for [{}] is {}", this.name, sender.getName(), sdpOffer);

    // loopback sdp
    String ipSdpAnswer = sdpOffer;

    // change candidate
    final String targetCandidate = "a=candidate:1 1 udp 1 0.0.0.0 10000 typ host generation 0\r\n";
    if (ipSdpAnswer.contains(targetCandidate)) {
      String candidate = "a=candidate:1 1 udp 1 1.232.90.69 " + String.valueOf(this.port) + " typ host generation 0";
      // log.info("USER [{}]: change candidate of [{}] -> {}", this.name, sender.getName(), candidate);
      String replace = candidate + "\r\n";

      ipSdpAnswer = ipSdpAnswer.replace(targetCandidate, replace);
    }

    if (sender.getName().equals(name)) {
      // sendonly

      // extract crypto line
      // final String[] lines = ipSdpAnswer.split("\\r\\n");
      // for (String line : lines) {
      //   String foundString = "a=crypto:0";
      //   if (line.contains(foundString)) {
      //     this.cryptoLine = line;
      //     log.info("USER [{}]: extract crypto line : {}", this.name, this.cryptoLine);
      //     break;
      //   }
      // }

      // change sendonly -> recvonly
      String foundString = "a=sendonly\r\n";
      if (ipSdpAnswer.contains(foundString)) {
        final String recvonly = "a=recvonly\r\n";
        ipSdpAnswer = ipSdpAnswer.replace(foundString, recvonly);
      }

      // send msg
      final JsonObject scParams = new JsonObject();
      scParams.addProperty("id", "receiveVideoAnswer");
      scParams.addProperty("name", sender.getName());
      scParams.addProperty("sdpAnswer", ipSdpAnswer);

      // add ssrcs only sendonly
      final JsonArray ssrcArray = new JsonArray();
      for (final String ssrc : this.ssrcs) {
        ssrcArray.add(new JsonPrimitive(ssrc));
      }
      scParams.add("ssrcs", ssrcArray); 

      log.trace("USER [{}]: sendonly offer's answer for [{}] is {}", this.name, sender.getName(), ipSdpAnswer);
      this.sendMessage(scParams);
    }
    else {
      // recvonly
      // append audio ssrc
      {
        final String ssrcCname = "a=ssrc:" + sender.getSsrcs().get(0) + " " + sender.getCname();
        final String ssrcMsid = "a=ssrc:" + sender.getSsrcs().get(0) + " " + sender.getAudiomsid();

        final String foundString = "m=video 10000 ";
        if (ipSdpAnswer.contains(foundString)) {
          final String audioSSrc = ssrcCname + "\r\n" + ssrcMsid + "\r\n" + foundString;

          // log.info("USER [{}]: [{}] added audio ssrc {}, {}", this.name, sender.getName(), ssrcCname, ssrcMsid);
          ipSdpAnswer = ipSdpAnswer.replace(foundString, audioSSrc);
        } else {
          final String audioSSrc = ssrcCname + "\r\n" + ssrcMsid + "\r\n";

          // log.info("USER [{}]: [{}] added audio ssrc {}, {}", this.name, sender.getName(), ssrcCname, ssrcMsid);
          ipSdpAnswer = ipSdpAnswer.concat(audioSSrc);
        }
      }

      // append video ssrc
      {
        final String foundString = "a=mid:video\r\n";
        if (ipSdpAnswer.contains(foundString)) {
          final String ssrcCname = "a=ssrc:" + sender.getSsrcs().get(1) + " " + sender.getCname();
          final String ssrcMsid = "a=ssrc:" + sender.getSsrcs().get(1) + " " + sender.getVideomsid();
          final String appStr = ssrcCname  + "\r\n" + ssrcMsid + "\r\n";

          // log.info("USER [{}]: [{}] added video ssrc {}, {}", this.name, sender.getName(), ssrcCname, ssrcMsid);
          ipSdpAnswer = ipSdpAnswer.concat(appStr);
        }
      }

      // change crypto line
      // final String[] lines = ipSdpAnswer.split("\\r\\n");
      // for (String line : lines) {
      //   foundString = "a=crypto:0";
      //   if (line.contains(foundString)) {
      //     String oldCrypto = line;
      //     String senderCrypto = sender.cryptoLine;

      //     log.info("USER [{}]: [{}] change crypto {} -> {}", this.name, sender.getName(), oldCrypto, senderCrypto);
      //     ipSdpAnswer = ipSdpAnswer.replace(oldCrypto, senderCrypto);
      //     break;
      //   }
      // }

      // change recvonly -> sendonly
      {
        final String foundString = "a=recvonly\r\n";
        if (ipSdpAnswer.contains(foundString)) {
          String recvonly = "a=sendonly\r\n";
          ipSdpAnswer = ipSdpAnswer.replace(foundString, recvonly);
        }
      }

      // send msg
      final JsonObject scParams = new JsonObject();
      scParams.addProperty("id", "receiveVideoAnswer");
      scParams.addProperty("name", sender.getName());
      scParams.addProperty("sdpAnswer", ipSdpAnswer);

      log.trace("USER [{}]: recvonly offer's answer for [{}] is {}", this.name, sender.getName(), ipSdpAnswer);
      this.sendMessage(scParams);
    }    
  }

  public void cancelVideoFrom(final UserSession sender) {
    this.cancelVideoFrom(sender.getName());
  }

  public void cancelVideoFrom(final String senderName) {
    log.debug("USER [{}]: canceling video reception from [{}]", this.name, senderName);
  }

  @Override
  public void close() throws IOException {
    log.debug("USER [{}]: Releasing resources", this.name);
  }

  public void sendMessage(JsonObject message) throws IOException {
    log.debug("USER [{}]: Sending message {}", name, message);
    synchronized (session) {
      session.sendMessage(new TextMessage(message.toString()));
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null || !(obj instanceof UserSession)) {
      return false;
    }
    UserSession other = (UserSession) obj;
    boolean eq = name.equals(other.name);
    eq &= roomName.equals(other.roomName);
    return eq;
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + name.hashCode();
    result = 31 * result + roomName.hashCode();
    return result;
  }
}
